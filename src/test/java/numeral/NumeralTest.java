package numeral;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NumeralTest {
    @Test
    public void test_initialisation(){
        Numeral num = new Numeral("5");
        assertEquals("5", num.getNum());
        num = new Numeral("785");
        assertEquals("785", num.getNum());
    }
    @Test
    public void test_to_letters_unity(){
        Numeral num = new Numeral("0");
        Numeral num2 = new Numeral("2");
        Numeral num3 = new Numeral("5");
        Numeral num4 = new Numeral("7");
    
        assertEquals("Zéro", num.toLetters());
        assertEquals("Deux", num2.toLetters());
        assertEquals("Cinq", num3.toLetters());
        assertEquals("Sept", num4.toLetters());
    }

    @Test
    public void test_to_letters_first_tens(){
        Numeral num = new Numeral("10");
        Numeral num2 = new Numeral("17");
        Numeral num3 = new Numeral("18");
        Numeral num4 = new Numeral("19");

        assertEquals("Dix", num.toLetters());
        assertEquals("Dix-sept", num2.toLetters());
        assertEquals("Dix-huit", num3.toLetters());
        assertEquals("Dix-neuf", num4.toLetters());
    }

    @Test
    public void test_harder_number_in_first_tens(){
        Numeral num = new Numeral("11");
        Numeral num2 = new Numeral("12");
        Numeral num3 = new Numeral("13");
        Numeral num4 = new Numeral("14");
        Numeral num5 = new Numeral("15");
        Numeral num6 = new Numeral("16");

        assertEquals("Onze", num.toLetters());
        assertEquals("Douze", num2.toLetters());
        assertEquals("Treize", num3.toLetters());
        assertEquals("Quatorze", num4.toLetters());
        assertEquals("Quinze", num5.toLetters());
        assertEquals("Seize", num6.toLetters());
    }

    
    @Test
    public void test_other_tens_basic_to_letter(){
        Numeral num = new Numeral("20");
        Numeral num2 = new Numeral("30");
        Numeral num3 = new Numeral("40");
        Numeral num4 = new Numeral("50");
        Numeral num5 = new Numeral("60");

        assertEquals("Vingt", num.toLetters());
        assertEquals("Trente", num2.toLetters());
        assertEquals("Quarante", num3.toLetters());
        assertEquals("Cinquante", num4.toLetters());
        assertEquals("Soixante", num5.toLetters());
    }

    @Test
    public void test_more_complex_basic_tens(){
        Numeral num = new Numeral("70");
        Numeral num2 = new Numeral("80");
        Numeral num3 = new Numeral("90");


        assertEquals("Soixante-dix", num.toLetters());
        assertEquals("Quatre-vingt", num2.toLetters());
        assertEquals("Quatre-vingt-dix", num3.toLetters());
    }
    
    @Test
    public void test_tens_with_numerals(){
        Numeral num = new Numeral("23");
        Numeral num2 = new Numeral("38");
        Numeral num3 = new Numeral("41");
        Numeral num4 = new Numeral("52");
        Numeral num5 = new Numeral("65");
        Numeral num6 = new Numeral("71");
        Numeral num7 = new Numeral("89");
        Numeral num8 = new Numeral("95");
        Numeral num9 = new Numeral("76");
        Numeral num10 = new Numeral("98");

        assertEquals("Vingt-trois", num.toLetters());
        assertEquals("Trente-huit", num2.toLetters());
        assertEquals("Quarante et un", num3.toLetters());
        assertEquals("Cinquante-deux", num4.toLetters());
        assertEquals("Soixante-cinq", num5.toLetters());
        assertEquals("Soixante et onze", num6.toLetters());
        assertEquals("Quatre-vingt-neuf", num7.toLetters());
        assertEquals("Quatre-vingt-quinze", num8.toLetters());
        assertEquals("Soixante-seize", num9.toLetters());
        assertEquals("Quatre-vingt-dix-huit", num10.toLetters());
    }

    @Test
    public void test_numeral_with_3_nums(){
        Numeral num = new Numeral("100");
        Numeral num2 = new Numeral("375");
        Numeral num3 = new Numeral("200");
        Numeral num4 = new Numeral("900");
        Numeral num5 = new Numeral("641");
        Numeral num6 = new Numeral("306");

        assertEquals("Cent", num.toLetters());
        assertEquals("Trois cent soixante-quinze", num2.toLetters());
        assertEquals("Deux cents", num3.toLetters());
        assertEquals("Neuf cents", num4.toLetters());
        assertEquals("Six cent quarante et un", num5.toLetters());
        assertEquals("Trois cent six", num6.toLetters());
    }
}
