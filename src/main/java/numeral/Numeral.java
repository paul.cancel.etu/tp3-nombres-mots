package numeral;

import java.util.HashMap;
import java.util.Map;

public class Numeral {
    private static final String SEPARATOR_ET = " et ";
    private static final String SEPARATOR_TIRET = "-";
    private String num;
    private static Map<String, String> numToLetters;
    private static Map<String, String> tens;

    private static final int CAPITAL_OFFSET = 32;

    private static final char MAX_LOWER_LETTER = 'z';

    private static final char MIN_LOWER_LETTER = 'a';

    static {
        Numeral.numToLetters = new HashMap<>();
        Numeral.numToLetters.put("0", "zéro");
        Numeral.numToLetters.put("1", "un");
        Numeral.numToLetters.put("2", "deux");
        Numeral.numToLetters.put("3", "trois");
        Numeral.numToLetters.put("4", "quatre");
        Numeral.numToLetters.put("5", "cinq");
        Numeral.numToLetters.put("6", "six");
        Numeral.numToLetters.put("7", "sept");
        Numeral.numToLetters.put("8", "huit");
        Numeral.numToLetters.put("9", "neuf");
        Numeral.numToLetters.put("11", "onze");
        Numeral.numToLetters.put("12", "douze");
        Numeral.numToLetters.put("13", "treize");
        Numeral.numToLetters.put("14", "quatorze");
        Numeral.numToLetters.put("15", "quinze");
        Numeral.numToLetters.put("16", "seize");
    }

    static {
        Numeral.tens = new HashMap<>();
        Numeral.tens.put("1", "dix");
        Numeral.tens.put("2", "vingt");
        Numeral.tens.put("3", "trente");
        Numeral.tens.put("4", "quarante");
        Numeral.tens.put("5", "cinquante");
        Numeral.tens.put("6", "soixante");
    }

    private static final String CENT = "cent";
    

    public Numeral(String number){
        this.num = number;
    }

    public String toLetters(){
        String res = "";
        if(Numeral.numToLetters.containsKey(this.num)){
            res = Numeral.numToLetters.get(this.num);
        }else if(this.num.length() == 2){
            String ten = ""+this.num.charAt(0);
            String numeral = ""+this.num.charAt(1);
            res = handleTens(ten, numeral);
        }else{
            String hundreds = ""+this.num.charAt(0);
            String ten = ""+this.num.charAt(1);
            String numeral = ""+this.num.charAt(2);
            res = handleHundreds(hundreds, ten, numeral);
        }
        return Numeral.uppercaseFirstLetter(res);
    }

    private String handleHundreds(String hundreds, String ten, String numeral) {
        String res;
        if(ten.equals("0")){
            if(numeral.equals("0")){
                res = exactHundreds(hundreds);
            }else{
                res = generateHundredsNoTens(hundreds, numeral);
            }
        }else{
            res = Numeral.numToLetters.get(hundreds) + " " + CENT + " " + handleTens(ten, numeral);
        }
        return res;
    }

    private String generateHundredsNoTens(String hundreds, String numeral) {
        String res;
        if(hundreds.equals("1")){
            res = CENT;
        }else{
            res = Numeral.numToLetters.get(hundreds) + " " + CENT + " " + Numeral.numToLetters.get(numeral);
        }
        return res;
    }

    private String exactHundreds(String hundreds) {
        String res;
        if(hundreds.equals("1")){
            res = CENT;
        }else{
            res = Numeral.numToLetters.get(hundreds) + " " + CENT + "s";
        }
        return res;
    }

    private String handleTens(String ten, String numeral) {
        switch (ten) {
            case "7":
                return generate70(numeral);
            case "8":
                return generate80(numeral);
            case "9":
                return generate90(numeral);
            default:
                return generateOther(ten, numeral);
        }
    }

    private String generateOther(String ten, String numeral) {
        switch (numeral) {
            case "0":
                return Numeral.tens.get(""+this.num.charAt(0)); 
            case "1":
                return Numeral.tens.get(ten)  + SEPARATOR_ET + Numeral.numToLetters.get(numeral);
            default:
                return Numeral.tens.get(ten)  + SEPARATOR_TIRET + Numeral.numToLetters.get(numeral);
        }
    }

    private String generate80(String digit) {
        if(digit.equals("0")){
            return Numeral.numToLetters.get("4") + SEPARATOR_TIRET + Numeral.tens.get("2");
        }else{
            return Numeral.numToLetters.get("4") + SEPARATOR_TIRET + Numeral.tens.get("2") + SEPARATOR_TIRET + Numeral.numToLetters.get(digit);
        }
    }

    private String generate70(String digit) {
        String res;
        switch (digit) {
            case "0":
                res = Numeral.tens.get("6") + SEPARATOR_TIRET + Numeral.tens.get("1");
                break;
            case "1":
                res = Numeral.tens.get("6") + SEPARATOR_ET + Numeral.numToLetters.get("1"+digit);
                break;
            case "7": case "8": case "9" : 
                res = Numeral.tens.get("6") + SEPARATOR_TIRET + Numeral.tens.get("1")  + SEPARATOR_TIRET + Numeral.numToLetters.get(digit);
                break;
            default:
                res = Numeral.tens.get("6") + SEPARATOR_TIRET + Numeral.numToLetters.get("1"+digit);
                break;
        }
        return res;
    }

    private String generate90(String digit){
        String res;
        switch (digit) {
            case "0":
                res = generate80("0") + SEPARATOR_TIRET + Numeral.tens.get("1");
                break;
            case "7": case "8": case "9" : 
                res = generate80("0") + SEPARATOR_TIRET + Numeral.tens.get("1")  + SEPARATOR_TIRET + Numeral.numToLetters.get(digit);
                break;
            default:
                res= generate80("0") + SEPARATOR_TIRET + Numeral.numToLetters.get("1"+digit);
                break;
        }
        return res;
    }

    public String getNum() {
        return this.num;
    }

    public static String uppercaseFirstLetter(String string) {
        char firstChar = string.charAt(0);

        if (firstChar >= MIN_LOWER_LETTER && firstChar <= MAX_LOWER_LETTER) {
            return (char) (firstChar - CAPITAL_OFFSET) + string.substring(1);
        } else {
            return string;
        }
    }
}